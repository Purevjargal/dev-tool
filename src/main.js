import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/mn'
import * as VueGoogleMaps from 'vue2-google-maps'
import Clipboard from 'v-clipboard'
import VJsoneditor from 'v-jsoneditor/src/index'

Vue.use(VJsoneditor)
Vue.config.productionTip = false

Vue.use(Clipboard)
Vue.use(VueGoogleMaps, {
  installComponents: true,
  load: {
    key: 'AIzaSyBMR0UOofrP0PrX4frgdj47ecBMDhEw4TM',
    region: 'MN',
    language: 'mn',
    libraries: 'places'
  }
})

Vue.use(ElementUI, { locale })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
